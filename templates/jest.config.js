module.exports = {
        preset: "jest-puppeteer",
        globals: {
            URL: "http://{{ website_address }}:{{ port_number }}"
        },
        testMatch: [
            "{{ stash_workdir }}/homepage.test.js"
        ],
        verbose: true
    }
